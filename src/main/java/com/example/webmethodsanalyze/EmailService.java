package com.example.webmethodsanalyze;

import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class EmailService {

    private static final Logger logger = Logger.getLogger(EmailService.class.getName());

    @Autowired
    private JavaMailSender emailSender;

    public void sendEmailWithAttachment(String to, String subject, String text, String pathToAttachment) throws MessagingException {
        try {
            MimeMessage message = emailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true);

            helper.setTo(to);
            helper.setSubject(subject);
            helper.setText(text);

            File file = new File(pathToAttachment);
            if (file.exists() && file.length() > 0) {
                helper.addAttachment(file.getName(), file);
                emailSender.send(message);
                logger.info("Email sent with attachment: " + file.getName());
            } else {
                logger.warning("No email sent - file is empty or does not exist.");
            }
        } catch (MessagingException e) {
            logger.log(Level.SEVERE, "Failed to send email: " + e.getMessage());
        }
    }
}
