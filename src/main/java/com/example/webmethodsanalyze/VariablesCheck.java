package com.example.webmethodsanalyze;

import java.io.FileInputStream;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class VariablesCheck {
	static Set<String> variables=new LinkedHashSet();
    private static final Logger logger = Logger.getLogger(WebMethodsAnalyzeApplication.class.getName());

	public static void traverseAndcheckVariablesDeleted(Node xmlfile) {
		
        if (xmlfile.getNodeType() == Node.ELEMENT_NODE) {
        	if(xmlfile.getNodeName().equals("value")&& xmlfile.getParentNode().getNodeName().equals("record")&& xmlfile.getParentNode().getParentNode().getNodeName().equals("array")) {
        		NamedNodeMap map=xmlfile.getAttributes();
        		/*for(int i=0; i<map.getLength();i++) {
        			System.out.println(map.item(i).getNodeName());
        		}*/
        		Node node1=map.item(0);
        		if(node1.getNodeValue().equals("field_name")) {
        			//System.out.println(node1.getNodeValue());
        			//System.out.println(xmlfile.getTextContent());
        			if(!xmlfile.getTextContent().startsWith("$")) {
        				variables.add(xmlfile.getTextContent());
        				System.out.println("Added :"+ xmlfile.getTextContent());
        			}
        			//variables.add(xmlfile.getTextContent());
        			//System.out.println("Added :"+ xmlfile.getTextContent());
        		}
        		
        	}
        	if(xmlfile.getNodeName().equals("MAPDELETE")) {
        		NamedNodeMap map1=xmlfile.getAttributes();
        		Node node2=map1.item(0);
        		String s=node2.getNodeValue();
        		int startIndex = s.indexOf('/') + 1; 
                int endIndex = s.indexOf(';');
                String sol=s.substring(startIndex, endIndex);
                System.out.println("Deleted :"+ sol);
                if(variables.contains(sol)) variables.remove(sol);
                //System.out.println(variables.size());
                
        	}
            //System.out.println(node.getNodeName());
            NodeList children = xmlfile.getChildNodes();
            for (int i = 0; i < children.getLength(); i++) {
            	traverseAndcheckVariablesDeleted(children.item(i));
                //if(children.item(i).getNodeName().equals("Values")) System.out.println("-------------------------rdr-------------");
            }
            
        }
    }
	
	public static Boolean checkUnusedVariable(Node xmlnode,String FileName)  {
		
            traverseAndgetUnusedVariables(xmlnode);
            if(variables.size()>0) {
            	variables.forEach(element ->             	
            	logger.severe("the flow service "+FileName+ " contains the unused variable :" + element)
            	);
            	return true;
            	//throw new VariablesException("the flow service "+FileName+" contains unused variables, check the report for more details");
            	
            }
            else {
            	
            	logger.info("the flow service "+FileName+ " contains no unused variable :");
            	return false;
            }
           
	}

	public static void traverseAndgetUnusedVariables(Node xmlfile) {
	
		if (xmlfile.getNodeType() == Node.ELEMENT_NODE) {
			if(xmlfile.getNodeName().equals("value")&& xmlfile.getParentNode().getNodeName().equals("record")&& xmlfile.getParentNode().getParentNode().getNodeName().equals("array")) {
				NamedNodeMap map=xmlfile.getAttributes();
    
				Node node1=map.item(0);
				if(node1.getNodeValue().equals("field_name")) {
    		
					if(!xmlfile.getTextContent().startsWith("$")) {
						variables.add(xmlfile.getTextContent());
    			}

    		}
    		
    	}
			if(xmlfile.getNodeName().equals("MAPCOPY")) {
    		NamedNodeMap map1=xmlfile.getAttributes();
    		for(int j=0; j<map1.getLength();j++) {
    			Node node2=map1.item(j);
        		String s=node2.getNodeValue();
        		int startIndex = s.indexOf('/') + 1; 
                int endIndex = s.indexOf(';');
                String sol=s.substring(startIndex, endIndex);
               
                if(variables.contains(sol)) variables.remove(sol);
    		}
    	}
    	
    	if(xmlfile.getNodeName().equals("MAPSET")) {
    		NamedNodeMap map1=xmlfile.getAttributes();
    		for(int j=0; j<map1.getLength();j++) {
    			Node node2=map1.item(j);
    			if(node2.getNodeName().equals("FIELD")) {
    				String s=node2.getNodeValue();
            		int startIndex = s.indexOf('/') + 1; 
                    int endIndex = s.indexOf(';');
                    String sol=s.substring(startIndex, endIndex);
                   
                    if(variables.contains(sol)) variables.remove(sol);
    			}
        		
    		}
    	}
    	
    	
    	
        
        NodeList children = xmlfile.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
        	traverseAndcheckVariablesDeleted(children.item(i));
        }
        
    }
}
	
	/*
	public static void traverseAndcheckVariables(Node xmlfile) {
		Set<String> variables=new LinkedHashSet();
        if (xmlfile.getNodeType() == Node.ELEMENT_NODE) {
        	if(xmlfile.getNodeName().equals("value") && xmlfile.getParentNode().getNodeName().equals("record")&& xmlfile.getParentNode().getParentNode().getNodeName().equals("array")) {
        		NamedNodeMap map=xmlfile.getAttributes();
        		/*for(int i=0; i<map.getLength();i++) {
        			System.out.println(map.item(i).getNodeName());
        		}*/
        		/*Node node1=map.item(0);
        		if(node1.getNodeValue().equals("field_name")) {
        			System.out.println(node1.getNodeValue());
        			System.out.println(xmlfile.getTextContent());
        			if(!xmlfile.getTextContent().startsWith("$")) {
        				variables.add(xmlfile.getTextContent());
        			}
        			variables.add(xmlfile.getTextContent());
        		}
        		
        	}
            //System.out.println(node.getNodeName());
            NodeList children = xmlfile.getChildNodes();
            for (int i = 0; i < children.getLength(); i++) {
            	traverseAndcheckVariables(children.item(i));
                //if(children.item(i).getNodeName().equals("Values")) System.out.println("-------------------------rdr-------------");
            }
        }
    }*/
	

}
