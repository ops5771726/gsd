package com.example.webmethodsanalyze;

import java.io.FileInputStream;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class InputOutputCheck {
	
	static Set<String> variables=new LinkedHashSet();
	static Set<String> outputVariables=new LinkedHashSet();
    private static final Logger logger = Logger.getLogger(WebMethodsAnalyzeApplication.class.getName());

	public static Boolean checkInputOutputValidator(Node node, String FileName) {
		
           
             if(traverseValidate(node)) {
            	 logger.info("the flow service "+FileName+ " is validating the Input and Output variable :");
             	return true;
             }
             else {
            	 logger.warning("the flow service "+FileName+ " isn't validating the Input and Output variable :");
            	 return false;
             }
       
		
	}
	
	public static Boolean traverseValidate(Node node) {
		if (node.getNodeType() == Node.ELEMENT_NODE) {
        	if(node.getNodeName().equals("Values")) {
        		
        		 NodeList children = node.getChildNodes();
        		 
                 for (int i = 0; i < children.getLength(); i++) {
                 	if(children.item(i).getNodeName().equals("value")&&(children.item(i).getAttributes().item(0).getNodeValue().equals("svc_in_validator_options")||children.item(i).getAttributes().item(0).getNodeValue().equals("svc_out_validator_options"))) {
                 		if(children.item(i).getTextContent().equals("none")) return false;
                 		
                 	}
                	 /*if(children.item(i).getNodeName().equals("value")) {
                    	 //System.out.println(children.item(i).getAttributes().item(0).getNodeValue());
                    	 if(children.item(i).getAttributes().item(0).getNodeValue().equals("svc_in_validator_options")||children.item(i).getAttributes().item(0).getNodeValue().equals("svc_out_validator_options")){
                        	 System.out.println(children.item(i).getTextContent());

                    	 }
                	 }*/
                 	//System.out.println(1);
                     //if(children.item(i).getNodeName().equals("Values")) System.out.println("-------------------------rdr-------------");
                 }
        		

        	}
            
        }
		return true;
	}
	
	
	
	
	public static Boolean checkOutputInputSpecified(Node ndfnode,String FileName) {
		

            
             if(!traverseAndcheckOutputInputExistence(ndfnode)) {
             	logger.severe("the flow service "+FileName+ " should specify the input and output variables.");
             	return false;
             }
             else {
            	 logger.info("the flow service "+FileName+ " has specify the input and output variables.");
            	 return true;
             }
            
            
        
		
	}
	
	public static Boolean traverseAndcheckOutputInputExistence(Node xmlfile) {
        if (xmlfile.getNodeType() == Node.ELEMENT_NODE) {
        	if(xmlfile.getNodeName().equals("array") && xmlfile.getParentNode().getAttributes().getNamedItem("name")!=null&& xmlfile.getParentNode().getAttributes().getNamedItem("name").getNodeValue().equals("sig_out")) {
        		if(xmlfile.getChildNodes().getLength()==0) {
        			return false;
        		}
        		
        	}
        	if(xmlfile.getNodeName().equals("array")&& xmlfile.getParentNode().getAttributes().getNamedItem("name")!=null && xmlfile.getParentNode().getAttributes().getNamedItem("name").getNodeValue().equals("sig_in")) {
        		if(xmlfile.getChildNodes().getLength()==0) {
        			return false;
        		}
        		
        	}
            //System.out.println(node.getNodeName());
            NodeList children = xmlfile.getChildNodes();
            for (int i = 0; i < children.getLength(); i++) {
            	traverseAndcheckOutputInputExistence(children.item(i));
                //if(children.item(i).getNodeName().equals("Values")) System.out.println("-------------------------rdr-------------");
            }
        }
        return true;
    }
	

	
	public static Boolean checkoutPutVariables(Node xmlnode,Node nfdnode,String FileName) {
		
		

            
            traverseAndcheckVariablesDeleted(xmlnode);
            traverseAndcheckOutput(nfdnode);
            
            if(outputVariables.size()!=variables.size()) {
            	
            	for (String element : variables) {
                    if (!outputVariables.contains(element)) {
                    	logger.severe("the flow service "+FileName+ " contains the unspecified Output variable :" + element);
                    	
                    }
                }
            	return true;
            }
       	 logger.info("the flow service "+FileName+ " return only specified output variables.");

            return false;
            
        
		
	}
	
	public static void traverseAndcheckOutput(Node xmlfile) {
        if (xmlfile.getNodeType() == Node.ELEMENT_NODE) {
        	
        	if(xmlfile.getNodeName().equals("value") && xmlfile.getParentNode().getNodeName().equals("record")&& xmlfile.getParentNode().getParentNode().getNodeName().equals("array")
        			) {
        		if(xmlfile.getParentNode().getParentNode().getParentNode().getAttributes().getNamedItem("name")!=null) {
            		if(xmlfile.getParentNode().getParentNode().getParentNode().getAttributes().getNamedItem("name").getNodeValue().equals("sig_out")) {
            			NamedNodeMap map=xmlfile.getAttributes();
                		
                		Node node1=map.item(0);
                		if(node1.getNodeValue().equals("field_name")) {
                				outputVariables.add(xmlfile.getTextContent());
                				
                
                		}
            		}

        		}
        	
        		
        	}
            
            NodeList children = xmlfile.getChildNodes();
            for (int i = 0; i < children.getLength(); i++) {
            	traverseAndcheckOutput(children.item(i));
            }
        }
    }
	
	
	
	public static void traverseAndcheckVariablesDeleted(Node xmlfile) {
		
        if (xmlfile.getNodeType() == Node.ELEMENT_NODE) {
        	if(xmlfile.getNodeName().equals("value")&& xmlfile.getParentNode().getNodeName().equals("record")&& xmlfile.getParentNode().getParentNode().getNodeName().equals("array")) {
        		NamedNodeMap map=xmlfile.getAttributes();
        		/*for(int i=0; i<map.getLength();i++) {
        			System.out.println(map.item(i).getNodeName());
        		}*/
        		Node node1=map.item(0);
        		if(node1.getNodeValue().equals("field_name")) {
        			//System.out.println(node1.getNodeValue());
        			//System.out.println(xmlfile.getTextContent());
        			if(!xmlfile.getTextContent().startsWith("$")) {
        				variables.add(xmlfile.getTextContent());
        			}
        			//variables.add(xmlfile.getTextContent());
        			//System.out.println("Added :"+ xmlfile.getTextContent());
        		}
        		
        	}
        	if(xmlfile.getNodeName().equals("MAPDELETE")) {
        		NamedNodeMap map1=xmlfile.getAttributes();
        		Node node2=map1.item(0);
        		String s=node2.getNodeValue();
        		int startIndex = s.indexOf('/') + 1; 
                int endIndex = s.indexOf(';');
                String sol=s.substring(startIndex, endIndex);
               
                if(variables.contains(sol)) variables.remove(sol);
                //System.out.println(variables.size());
                
        	}
            //System.out.println(node.getNodeName());
            NodeList children = xmlfile.getChildNodes();
            for (int i = 0; i < children.getLength(); i++) {
            	traverseAndcheckVariablesDeleted(children.item(i));
                //if(children.item(i).getNodeName().equals("Values")) System.out.println("-------------------------rdr-------------");
            }
            
        }
    }

}
