package com.example.webmethodsanalyze;

public class PackageStructureVerifierException extends Exception {
    public PackageStructureVerifierException() {
        super();
    }


    public PackageStructureVerifierException(String message) {
        super(message);
    }
}