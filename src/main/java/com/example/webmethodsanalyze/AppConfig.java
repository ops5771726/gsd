package com.example.webmethodsanalyze;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

    @Value("${connection.package:false}")
    private boolean connectionPackage;

    @Value("${adapter.package:false}")
    private boolean adapterPackage;

    public boolean isConnectionPackage() {
        return connectionPackage;
    }
}
